Projet Intensif Ensicaen
==

Ce projet construit lors de première semaine de janvier 2016 regroupe des étudiants issues de différentes écoles supérieures de Caen (Université de Caen, Ensicaen, Lycée Laplace et Lycée Brassard) et à permis l'élaboration d'un projet innovant autour de la thématique "Quelles nouvelles fonctions peut offrir le numérique au mobilier urbain".

## Biocité, cultive ta ville
Biocité est un projet visant à faciliter l'entretien des jardins publics et à éduquer les citoyens autour du thème de la cultivation des plantes.

## Equipe
Codou Bèye
Emilie Louvat
Quentin Derory
Peter Marsh
Valentin Laforge
Anas Rabhi
Yassine Chemingui
Francis Laforge
Jean-Laurent Ruiz
Rébecca Motel
Soline Arnaud
Lauriane Masson
Vivien Vieceli

Un descriptif des fichiers du dépôt est présent dans le fichier checklist.pdf