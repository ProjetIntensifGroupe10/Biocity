INSERT INTO Adresse (idAdresse, ville, CP, rue, numero, complement) VALUES
(1, 'Caen', '14000', 'bl lyautey', 12, 'Na'),
(2, 'Caen', '14000', 'bl lyautey', 12, 'Na');


INSERT INTO Citoyen (idCitoyen, nom, prenom, points, adresse) VALUES
(1, 'Peter', 'Marsh', '5', 1),
(2, 'Tim', 'bob', '0', 2);


INSERT INTO Contient (culture_id, plante_id) VALUES
(1, 1);


INSERT INTO Culture (idCulture, nom, dateCreation) VALUES
(1, 'Culture de Caen', '2016-01-06 08:34:43'),
(2, 'Culture de ifs', '2016-01-06 14:43:56');


INSERT INTO Plante (idPlante, planteDesc, dateCreation) VALUES
(1, 1, '2016-01-06 08:38:46');



INSERT INTO PlanteDescription (idPlanteDescription, nomCommun, nomScientifique, description, densite, ensoleillement) VALUES
(1, 'carrot', 'Daucus carota subsp. sativus', 'legume orange', '5', 'oui'),
(2, 'pomme de terre', 'Solanum tuberosum', 'legume', '3', 'non');


INSERT INTO Pousse (saison_id, planteDesc_id) VALUES
('ete', 2);


INSERT INTO Saison (nom) VALUES
('automne'),
('ete'),
('hiver'),
('printemps');


INSERT INTO Type (categorie) VALUES
('aromate'),
('divers'),
('fleur'),
('legume');



INSERT INTO Utilisateur (pseudo, password, citoyen_id, email) VALUES
('carrot', '$2y$10$Z4SDUOU2E6503zni.DuMHe3Sd2fj86EcG7hZJr', 1, '21005385@etu.unicaen.fr'),
('harry', '$2y$10$RzHLwfv1mHYV1YeEyaYhPO4HuibkZ8WZf2GsSr', 4, 'peter.marsh@wanadoo.fr');
