$(document).on("click", "#cultures",function (ev){
            $.ajax({
                type: "POST",/*method type*/
                url: "http://localhost/backend/getPlantesCulture.php", 
                data: { 
                    "nomCulture" : $("#nomCulture").val(),
                    },
                dataType: "json",
                success: function(data) {
                    html = " "
                    $.each(data, function( index, value ){
						id = value["Numero"]
						nom = value["Nom commun"]
						science = value["Nom Scientifique"]
						desc = value["Description"]
						saison = value["Saison"]
						densite = value["Densité"]
						html += "<div><br/> Numéro de plante : " + id + "<br/> Nom commun : " + nom + "<br> Description  : " + desc + "</div>"
					});
					
                    $("#results").html(html)
                }
                ,
                error: function(resultat) {}
            });  
   });  


$(document).on("click", "#adopte",function (ev){
            $.ajax({
                type: "POST",/*method type*/
                url: "http://localhost/backend/adoptePlante.php", 
                data: { 
                    "pseudo" : $("#pseudo").val(),
                    "idPlante" : $("#idPlante").val(),
                    },
                dataType: "json",
                success: function(data) {
					alert("Bravo, vous avez adopté votre plante");
                }
                ,
                error: function(resultat) {}
            });  
}); 


$(document).ready(function (ev){
	 $.ajax({
                type: "POST",/*method type*/
                url: "http://localhost/backend/getCultures.php", 
                dataType: "json",
                success: function(data) {
					html = "";
					$.each(data, function( index, value ){
						html += "<div class=\"row\">" + 
							"<div class=\"col-md-7\">" +
									"<a href=\"portfolio-item.html\">" +
										"<img class=\"img-responsive img-hover\" src=\"../img/mur1.jpg\" height=\"600px\" width=\"600px\" alt=\"\">" +
									"</a>" +
								"</div>" +
							   "<div class=\"col-md-5\">" +
									"<h3 id=\"sqds\">" + value["nom"] + "</h3>" +
									"<h4>Emplacement numéro : " + value["id"] + "</h4>" +
									"<h4 style=\"visibility:hidden\">" + value["id"] + "</h4>" +
									"<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium veniam exercitationem expedita laborum at voluptate. Labore, voluptates totam at aut nemo deserunt rem magni pariatur quos perspiciatis atque eveniet unde.</p>" +
									"\n<a id=\"information\" class=\"btn btn-primary\" href=\"#\">Informations</i></a>" +
									"\n<a id=\"interaction\" class=\"btn btn-primary\" href=\"#\">Adopter</i></a>" +
									"\n<a id=\"interaction\" class=\"btn btn-primary\" href=\"#\">Réserver</i></a>" +
								"</div>" +
								
							"</div>" + 
							"<div id=\"plantes-" + value["id"] + "\"></div>" +
							"<hr>";
					});
					
					$("#results").html(html);
										
                }
                ,
                error: function(resultat) {}
            });  
           
});

$(document).on("click", "#information", function (ev){
	ev.preventDefault();
	id = "#plantes-" + $(ev.target).prevAll('h4').html();
	if($(id).html() != ""){
		$(id).html("");
	}
	else{
		$.ajax({
			type: "POST",/*method type*/
			url: "http://localhost/backend/getPlantesCulture.php", 
			data: { 
				"nomCulture" : $(ev.target).prevAll('h3').html(),
				},
			dataType: "json",
			success: function(data) {
				html = "<hr><ul style=\"list-style:none\">";
				$.each(data, function( index, value ){
					html += "<li>" +
						"<div id=\"" +value["Numero"]+ "\" style=\"display:inline; font-size : 7em;\">"+value["Numero"]+"</div>" +
						"<fieldset style=\"display:inline;\">" +
							"<legend>"+value["Nom commun"]+"</legend>" +
							"<ul>"+
								"<li>Description : "	+value["Description"]+ 		"</li>" +
								"<li>Nombre de plantes maximum : "			+value["Densite"]+			"</li>" +
								"<li>Saison : "			+value["Saison"] +			"</li>" +
							"</ul>" +
						"</fieldset>" +
					"</li>";
				});
				
				html+="</ul>";
				id = "#plantes-" + $(ev.target).prevAll('h4').html();
				$(id).html(html);
									
			}
			,
			error: function(resultat) {}
		});  
	}
});





$(document).on("click", "#inscription", function (ev){
	ev.preventDefault();
	$.ajax({
		type: "POST",/*method type*/
		url: "http://localhost/backend/signup.php", 
		data: { 
			"pseudo" : $('#pseudo').val(),
			"nom" : $('#nom').val(),
			"prenom" : $('#prenom').val(),
			"password" : $('#password').val(),
			"email" : $('#email').val(),
			"numero" : $('#numero').val(),
			"rue" : $('#rue').val(),
			"complement" : $('#complement').val(),
			"cp" : $('#cp').val(),
			"ville" : $('#ville').val()
			},
		dataType: "json",
		success: function(data) {
			if(data["success"] == 1){
				alert("Bravo, vous etes inscrit");
				window.location.href = 'index.html';
			}
			else{
				alert("Opps, something went wrong");
			}
								
		}
		,
		error: function(resultat) {}
	});  
});  
































