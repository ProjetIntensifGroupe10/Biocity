<?php
	/*
	* Fichier qui permet à un utilisateur de s'inscrire
	*/

	/**
	*Inscrit un utilisateur selon des contraintes sur le pseudo et le mot de passe
	*
	*@param pseudo
	*@param password
	*@param nom
	*@param prenom
	*@param complement
	*@param cp
	*@param numero
	*@param rue
	*@param ville
	*@param email
	*@return reply erreur et message d'erreur en fonction de si l'on n'a pas pu ajouter la culture sinon on retourne un succes
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	$adresse = array();
	if (isset($_POST['pseudo'])) $pseudo = $_POST['pseudo'];
	if (isset($_POST['password'])) $password = $_POST['password'];
	if (isset($_POST['nom']))  $nom = $_POST['nom'];
	if (isset($_POST['prenom']))  $prenom = $_POST['prenom'];
	if (isset($_POST['complement']))  $adresse['complement'] = $_POST['complement'];
	if (isset($_POST['cp']))  $adresse['cp'] = $_POST['cp'];
	if (isset($_POST['numero']))  $adresse['numero'] = $_POST['numero'];
	if (isset($_POST['rue']))  $adresse['rue'] = $_POST['rue'];
	if (isset($_POST['ville']))  $adresse['ville'] = $_POST['ville'];
	if (isset($_POST['email']))  $email = $_POST['email'];

	//Si un nom d'utilisateur et un mot de passe sont fournis
    	if(isset($pseudo) && isset($password)){
		//test si le nom d'utilisateur est vide et si le mot passe est plus long que 6 caractères
		if($pseudo==""){
			$reply['error'] = 1;
			$reply['error_message'] = "Nom d'utilisateur vide";
		}
		else{
			//Crée l'utilisateur dans la base de donnée
			Outils_bd::add_user($pseudo, $nom, $prenom, $adresse, $password, $email);
			$reply["success"] = 1;
		}
    }
    else{
		$reply["error"] = 1;
		$reply["error_message"] = "Une erreur";
    }    
	echo json_encode($reply);    
?>
