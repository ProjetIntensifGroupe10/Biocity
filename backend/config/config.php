<?php
/*
* Fichier de config pour le server MYSQL et le backend en général.
* Définit les variables globales utilisées par l'intégralité du backend,
* ainsi que la config pour se connecter au server mysql.
*/

	//Meta donnée pour l'envoie de requete POST entre front et backend
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Content-Type');
	
	//Détails du server mysql
	define("SERVER","127.0.0.1");
	define('USER',"user10");
	define('PASSWD',"7AKU2TN8KLmtrqEH");
	define('DB_NAME', "biocite");
	define('PORT', "3306");
	define('PDO_DSN',"mysql:host=" . SERVER . ";port=" . PORT . ";dbname=" . DB_NAME);
	
	//Variables globales
	define('__PASSWORD_LENGTH', 6);
	ini_set('display_errors', '1');

?>
