<?php
require_once('lib/password.php');
/**
 * Classe qui gère les appels de fonction à une base de données, par ex
 * l'ajout des utilisateurs ou encore la suppression d'un utilisateur.
 *
 * @author Peter Marsh
 */
class Outils_bd {
    static private $pdo = false;
	static private $citoyen_table = 'Citoyen';
	static private $utilisateur_table = 'Utilisateur';
	static private $adresse_table = 'Adresse';
	static private $adopte_table = 'Adopte';
	static private $plante_table = 'Plante';
	static private $description_plante_table = 'PlanteDescription';
	static private $occupe_table = 'Occupe';
	static private $possede_table = 'Possede';
	static private $culture_table = 'Culture';
	static private $contient_table = 'Contient';
	static private $saison_table = 'Saison';
	static private $pousse_table = 'Pousse';
	static private $hashing_cost = 10;
	

	static public function getPDO() {
		/**
		 * Singleton PDO, le pdo représente une connexion entre PHP et un serveur de base de données. 
		 * @return PDO object 
		*/
		if (self::$pdo) {
			/* l'objet PDO a déjà été instancié */
	    	return self::$pdo;
		} 
		else {
			/* création d'un objet PDO avec les constantes définies dans la configuration */
		    self::$pdo = new PDO(PDO_DSN, USER, PASSWD);
		   	/* mettre Exception comme mode d'erreur */
		   	self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return self::$pdo;
		}
	}
	

	static public function get_Utilisateur($pseudo) { 
		/**
		*	Récupération des informations d'un utilisateur en fonction
		* 	de son pseudo.
		*	@param $pseudo d'un utilisateur (equivalent à login)
		*	@return utilisateur array du type [id, pseudo, nom, prenom, points, ville, email] 
		*/
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("SELECT * FROM ".self::$utilisateur_table." AS a LEFT JOIN ".self::$citoyen_table." b ON a.citoyen_id = b.idCitoyen
						LEFT JOIN ".self::$adresse_table." c  ON c.idAdresse = b.adresse
						WHERE a.pseudo=:pseudo");
		$sth->execute(array(
				':pseudo' => $pseudo,
		));
		$ligne = $sth->fetch(PDO::FETCH_ASSOC);
		$utilisateur = array(
				'id' 	   => $ligne["citoyen_id"],
				'pseudo' => $ligne["pseudo"],
				'nom' => $ligne["nom"],
				'prenom'   => $ligne['prenom'],
				'points'   => $ligne['points'],
				'ville' => $ligne['ville'],
				'email' => $ligne['email']
		);
		return $utilisateur;
	}
	
	
	static public function add_user($pseudo, $nom, $prenom, $adresse, $password, $email) {
		/**
		*	Insertion d'un utilisateur dans la bdd.
		*	@param $pseudo, $nom, $prenom, $adresse, $password, $email
		*	@return Boolean
		*/
		$db = Outils_bd::getPDO();
		
		/*generation du hash d'un mdp*/
		$password = Outils_bd::hash_pass($password);
		
		/*Ajoute adresse et retourne l'id de l'adresse ajouter*/
		$adresse_id = Outils_bd::add_adresse($adresse);
		/*Ajoute citoyen et retourne l'id du cityoen ajouter*/
		$citoyen_id = Outils_bd::add_citoyen($nom, $prenom, $adresse_id);

		$sth=$db->prepare("INSERT INTO ".self::$utilisateur_table." (pseudo, password, citoyen_id, email) VALUES(:pseudo, :password, :citoyen_id, :email)");
		$sth->execute(array(
			':pseudo' => $pseudo,
			':password' => $password,
			':citoyen_id' => $citoyen_id,
			':email' => $email,
		));
		return true;
	
	}


	static public function add_citoyen($nom, $prenom, $adresse_id) {
		/**
		*	Ajout d'un citoyen dans la bdd.
		* 	@param $nom, $prenom, $adresse_id
		* 	@return id du citoyen insere
		*/
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("INSERT INTO ".self::$citoyen_table." (nom, prenom, points, adresse) VALUES(:nom, :prenom, :points, :adresse)");
		$sth->execute(array(
			':nom' => $nom,
			':prenom' => $prenom,
			':points' => 0,
			':adresse' => $adresse_id,
		));
		return $db->lastInsertId();
	
	}


	static public function add_adresse($adresse) {
		/**
		 * Ajoute une adresse dans la bdd.
		 * @param $adresse (format array)
		 * @return id de l'adresse insere
		*/
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("INSERT INTO ".self::$adresse_table." (ville, cp, rue, numero, complement) VALUES(:ville, :cp, :rue, :numero, :complement)");
		$sth->execute(array(
			':ville' => $adresse['ville'],
			':cp' => $adresse['cp'],
			':rue' => $adresse['rue'],
			':numero' => $adresse['numero'],
			':complement' => $adresse['complement']			
		));
		return $db->lastInsertId();
	}
	
	static public function add_culture($nom) {
		/**
		 * Ajoute une culture dans la bdd
		 * @param $nom
		 * @return Boolean
		*/
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("INSERT INTO ".self::$culture_table." (nom, dateCreation) VALUES(:nom, CURRENT_TIMESTAMP)");
		$sth->execute(array(
			':nom' => $nom,		
		));
		return true;
	}
	
	static public function add_plant($nomCommun, $nomScientifique, $description, $densite, $ensoleillement, $saison){
		/**
		 * Ajoute une plante dans la base de donné
		 * @param $nomCommun, $nomScientifique, $description, $densite, $ensoleillement, $saison
		 * @return Boolean
		*/
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("INSERT INTO ".self::$description_plante_table."(nomCommun, nomScientifique, description, densite, ensoleillement) VALUES(:nomCommun
						, :nomScientifique, :description, :densite, :ensoleillement)");
		$sth->execute(array(
			':nomCommun' => $nomCommun,
			':nomScientifique' => $nomScientifique,
			':description' => $description,
			':densite' => $densite,
			':ensoleillement' => $ensoleillement,
		));
		$planteDescId = $db->lastInsertId();
		
		/*Attache une plante à une saison*/
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("INSERT INTO ".self::$pousse_table." (saison_id, planteDesc_id) VALUES(:saison_id, :palnteDesc_id)");
		$sth->execute(array(
			':saison_id' => $saison,
			':palnteDesc_id' => $planteDescId			
		));
		return true;
	}

	
	static public function get_plant_description($nomCommun){
		/**
		 * Récupère les informations d'une plante
		 * @param $nomCommun
		 * @return array
		 * */
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("SELECT * FROM ".self::$description_plante_table." WHERE nomCommun = :nomCommun");
		$sth->execute(array(
				':nomCommun' => $nomCommun,
		));
		$ligne = $sth->fetch(PDO::FETCH_ASSOC);
		$plant = array(
				'id' 	   => $ligne["idPlanteDescription"],
				'nomCommun' => $ligne["nomCommun"],
				'nomScientifique' => $ligne["nomScientifique"],
				'description'   => $ligne['description'],
				'densite'   => $ligne['densite'],
				'ensoleillement' => $ligne['ensoleillement']
		);
		return $plant;
	}
	
	
	static public function get_culture($nom){
		/**
		 * Récupère les informations d'une culture.
		 * @param $nom
		 * @return array
		 * */
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("SELECT * FROM ".self::$culture_table." WHERE nom = :nom");
		$sth->execute(array(
				':nom' => $nom,
		));
		$ligne = $sth->fetch(PDO::FETCH_ASSOC);
		$culture = array(
				'id' 	   => $ligne["idCulture"],
				'nom' => $ligne["nom"],
				'dateCreation' => $ligne["dateCreation"]
		);
		return $culture;
	}
	
	
	static public function get_culture_plants($nom){
		/**
		 * 
		 * Récupère les information des differents plantes associé à
		 * une culture.
		 * @param $nom
		 * @return array
		 * */
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("SELECT * FROM ".self::$description_plante_table." AS a LEFT JOIN ".self::$plante_table." b ON a.idPlanteDescription = b.planteDesc
						LEFT JOIN ".self::$contient_table." c ON c.plante_id = b.idPlante LEFT JOIN ".self::$culture_table." d ON c.culture_id = d.idCulture
						LEFT JOIN ".self::$pousse_table." e ON e.planteDesc_id = a.idPlanteDescription 
						WHERE d.nom=:nom");
		$sth->execute(array(
				':nom' => $nom,
		));
		$ligne = $sth->fetchAll();
		$plants = array();
		foreach ($ligne as $value) {
			array_push($plants, array(
					'Numero' => $value["idPlante"], 
					'Nom commun' => $value["nomCommun"], 
					'Nom scientifique' => $value["nomScientifique"], 
					'Description' => $value["description"], 
					'Densite' => $value["densite"], 
					'Saison' => $value["saison_id"]));
		}
		return $plants;
	}
	
	
	static public function get_cultures(){
		/**
		 * Récupère toutes les cultures existante dans la bdd.
		 * @return array
		 * */
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("SELECT * FROM ".self::$culture_table);
		$sth->execute();
		$ligne = $sth->fetchAll();
		$cultures = array();
		foreach ($ligne as $value) {
			array_push($cultures, array(
				'id' => $value["idCulture"],
				'nom' => $value["nom"],
				'dateCreation' => $value["dateCreation"]
			));
		}
		return $cultures;
	}
	
	
	
	static public function plant_plant($nomCulture, $nomPlante) {
		/**
		 * Attacher une plante à une culture dans la bdd
		 * @param $nomculture, $nomPlante
		 * @return Boolean
		*/
		$db = Outils_bd::getPDO();
		$planteDesc = Outils_bd::get_plant_description($nomPlante);
		$culture = Outils_bd::get_culture($nomCulture);
		$sth=$db->prepare("INSERT INTO ".self::$plante_table." (planteDesc, dateCreation) VALUES(:planteDescId, CURRENT_TIMESTAMP)");
		$sth->execute(array(
			':planteDescId' => $planteDesc["id"],
		));
		$plantId = $db->lastInsertId();
		
		$sth=$db->prepare("INSERT INTO ".self::$contient_table." (culture_id, plante_id) VALUES(:culture_id, :plante_id)");
		$sth->execute(array(
			':culture_id' => $culture["id"],
			':plante_id' => $plantId
		));
		return true;
		
	}


	static public function get_plants(){
		/**
		 * Récupere les differents type de plante dans la bdd
		 * @return Une array de plantes 
		 * */
		$db = Outils_bd::getPDO();
		$sth=$db->prepare("SELECT nomCommun FROM ".self::$description_plante_table);
		$sth->execute();
		$ligne = $sth->fetchAll();
		$plants = array();
		foreach ($ligne as $value) {
			array_push($plants, $value["nomCommun"]);
		}
		return $plants;
	}
	
	static public function adopte_plant($pseudo, $planteId){
		/**
		 * Attacher une culture à un citoyen
		 * @param $pseudo, $planteId
		 * @return Boolean
		 * */
		$db = Outils_bd::getPDO();
		$user = outils_bd::get_Utilisateur($pseudo);
		$sth=$db->prepare("INSERT INTO ".self::$adopte_table." (citoyen_id, plante_id, date_adoption) VALUES(:citoyen_id, :plante_id, CURRENT_TIMESTAMP)");
		$sth->execute(array(
			':citoyen_id' => $user["id"],
			':plante_id' => $planteId
		));
		return true;
	}
	
	/**
	 * 
	 * Functions optionnels
	 * 
	 * */
	static public function hash_pass($password){
		/**
		*	Crée le hash d'un mot de passe
		*	@param $password
		*	@return Hashé du mot de passe donnée en paramètre using BCRYPT
		*/
		$hash = password_hash($password, PASSWORD_BCRYPT, array("cost" => self::$hashing_cost));
		return $hash;
	}
	


	static public function change_password($username, $new_pass){
		/**
		*	Change le mot de passe d'un utilisateur.
		* 	@param $username
		* 	@param $new_pass, nouveau mot de passe
		*/
		$db = Outils_bd::getPDO();
		$sth = $db->prepare("UPDATE ".self::$users_table." SET password=:password WHERE username=:username");
		$new_pass_hash = Outils_bd::hash_pass($new_pass);
		$sth->execute(array('username' => $username, 'password' => $new_pass_hash));
	}

}
?>
