<?php
	/**
	* Fichier qui permet de récupérer l'ensemble des plantes pour une culture donnée dans la base de données
	*
	*@author Peter Marsh
	*/

	/**
	*Renvoie les plantes existantes pour une culuture dans la base de données si le nom de la culture est valable, retourne une erreur sinon
	*
	*@param nomCulture
	*@return culturePlants
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	if (isset($_POST['nomCulture'])) $nomCulture = $_POST['nomCulture'];
	
    if(isset($nomCulture)){
		if($nomCulture==""){
			$reply['error'] = 1;
			$reply['error_message'] = "";
		}
		else{
			$culturePlants = Outils_bd::get_culture_plants($nomCulture);
			$reply["success"] = 1;
		}
    }
    else{
		$reply["error"] = 1;
		$reply["error_message"] = "Une erreur";
    }    
	echo json_encode($culturePlants);    
?>
