<?php
	/**
	* Fichier qui permet de récupérer l'ensemble des cultures dans la base de données
	*
	*@author Peter Marsh
	*/

	/**
	*Renvoie les cultures existantes dans la base de données
	*
	*@return cultures
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');

	$cultures = Outils_bd::get_cultures();
	
	echo json_encode($cultures);    
?>
