<?php
	/*
	* Fichier qui permet à un utilisateur de s'inscrire dans la base de données
	*/

	//Récupére les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	if (isset($_POST['pseudo'])) $pseudo = $_POST['pseudo'];
	if (isset($_POST['action'])) $action = $_POST['action'];

	//Si un nom d'utilisateur et un mot de passe sont fournis
    	if(isset($pseudo)){
		//test si le nom d'utilisateur est vide et si le mot passe est plus long que 6 caractères
		if($pseudo=="" && $action == ""){
			$reply['error'] = 1;
			$reply['error_message'] = "Pas de pseudo/action";
		}
		else{
			$user = Outils_bd::update_score($pseudo, $action);
			$reply["success"] = 1;
		}
    }
    else{
		$reply["error"] = 1;
		$reply["error_message"] = "Une erreur";
    }    
	echo json_encode($reply);    
?>
