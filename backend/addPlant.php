<?php
	/**
	*Fonction php pour ajouter une plante
	*
	*@author Peter Marsh
	*/

	/**
	*Teste si les valeur reçues sont valables, les ajoute ou renvoie une erreur le cas échéant
	*
	*@param nomCommun paramètre nécéssaire pour l'ajout d'une plante
	*@param nomScientifique paramètre facultatif
	*@param description paramètre nécéssaire pour l'ajout d'une plante
	*@param densite nombre de plantes que l'on peut planter par mètre carré, paramètre facultatif
	*@param ensoleillement paramètre facultatif
	*@param saison paramètre facultatif
	*@return reply erreur et message d'erreur en fonction de si l'on n'a pas pu ajouter la culture sinon on retourne un succes
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	if (isset($_POST['nomCommun'])) $nomCommun = $_POST['nomCommun'];
	if (isset($_POST['nomScientifique'])) $nomScientifique = $_POST['nomScientifique'];
	if (isset($_POST['description'])) $description = $_POST['description'];
	if (isset($_POST['densite'])) $densite = $_POST['densite'];
	if (isset($_POST['ensoleillement'])) $ensoleillement = $_POST['ensoleillement'];
	if (isset($_POST['saison'])) $saison = $_POST['saison'];
	
    if(isset($nomCommun)){
		if($nomCommun=="" && $description == ""){
			$reply['error'] = 1;
			$reply['error_message'] = "Veuillez ajouté une description";
		}
		else{
			$user = Outils_bd::add_plant($nomCommun, $nomScientifique, $description, $densite, $ensoleillement, $saison);
			$reply["success"] = 1;
		}
    }
    else{
		$reply["error"] = 1;
		$reply["error_message"] = "Une erreur";
    }    
	echo json_encode($reply);    
?>
