<?php
	/**
	* Fichier qui permet de récupérer l'ensemble des plantes dans la base de données
	*
	*@author Peter Marsh
	*/

	/**
	*Renvoie les plantes existantes dans la base de données
	*
	*@return plantes
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');

	$plantes = Outils_bd::get_plants();
	
	echo json_encode($plantes);    
?>
