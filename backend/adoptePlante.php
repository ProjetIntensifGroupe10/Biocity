<?php
	/**
	* Fichier qui permet à un utilisateur d'adopter un emplacement végétal
	*
	*@author Peter Marsh
	*/

	/**
	*Teste si les identifiants pour l'emplacement des plantes et le pseudo sont valides, l'adoption ne sera pas possible le cas échéant
	*
	*@param idPlante identifiant pour l'emplacement contenant une ou plusieurs plantes
	*@param pseudo l'utilisateur doit être inscrit et avoir un pseudo pour pouvoir adopter un emplacement
	*@return reply erreur et message d'erreur en fonction de si l'on n'a pas pu ajouter la culture sinon on retourne un succes
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	if (isset($_POST['nomCulture'])) $nomCulture = $_POST['nomCulture'];
	if (isset($_POST['pseudo'])) $pseudo = $_POST['pseudo'];

	//Si un nom d'utilisateur et un mot de passe sont fournis
    	if(isset($pseudo) && isset($idPlante)){
		//test si le nom d'utilisateur est vide et si le mot passe est plus long que 6 caractères
		if($pseudo=="" && $idPlante == ""){
			$reply['error'] = 1;
			$reply['error_message'] = "Pas de pseudo/idPlante";
		}
		else{
			$user = Outils_bd::adopte_plant($pseudo, $nomCulture);
			$reply["success"] = 1;
		}
    }
    else{
		$reply["error"] = 1;
		$reply["error_message"] = "Une erreur";
    }    
	echo json_encode($reply);    
?>
