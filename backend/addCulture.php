<?php
	/**
	*Fonction php pour ajouter une culture
	*
	*@author Peter Marsh
	*/

	/**
	*Teste si le nom de la culture reçu est valide,
	*ajoute la culture si c'est le cas, retourne un message d'erreur sinon
	*
	*@param nomCulture nom de la culture à ajouter
	*@return reply erreur et message d'erreur en fonction de si l'on n'a pas pu ajouter la culture sinon on retourne un succes
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	if (isset($_POST['nomCulture'])) $nomCulture = $_POST['nomCulture'];
	
    if(isset($nomCulture)){
		if($nomCulture==""){
			$reply['error'] = 1;
			$reply['error_message'] = "";
		}
		else{
			$reponse = Outils_bd::add_culture($nomCulture);
			$reply["success"] = 1;
		}
    }
    else{
		$reply["error"] = 1;
		$reply["error_message"] = "Une erreur";
    }    
	echo json_encode($reply);    
?>
