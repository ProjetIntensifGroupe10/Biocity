<?php
	/**
	* Fichier qui permet de récupérer l'ensemble des plantes pour une saison donnée dans la base de données
	*
	*@author Peter Marsh
	*/

	/**
	*Renvoie les plantes existantes pour une saison dans la base de données si le nom de la culture est valable, retourne une erreur sinon
	*
	*@return reply
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	$saisons = Outils_bd::get_saisons();
	$plants = Outils_bd::get_plants();
	$plantsCult = Outils_bd::get_culture_plants("Culture de caen");
	$reply["success"] = 1;
	$reply["saisons"] = $saisons;
	$reply["plants"] = $plants;
	$reply["culture"] = $plantsCult;
	echo json_encode($reply);    
?>
