<?php
	/**
	* Fichier qui à utilisateur permet d'ajouter une plante dans une culture
	*
	*@author Peter Marsh
	*/

	/**
	*Teste si le nom de la plante et la culture sont valides, si c'est le cas on ajoute la plante à la culture, sinon retourne un message d'erreur
	*
	*@param nomCulture
	*@param nomPlante
	*@return reply erreur et message d'erreur en fonction de si l'on n'a pas pu ajouter la culture sinon on retourne un succes
	*/
	
	//Récupère les fichiers de config
	require_once('config/config.php');
	require_once('lib/outils_bd.php');
	
	$reply = array("success" => 0, "error" => 0);
	if (isset($_POST['nomCulture'])) $nomCulture = $_POST['nomCulture'];
	if (isset($_POST['nomPlante'])) $nomPlante = $_POST['nomPlante'];

	//Si un nom d'utilisateur et un mot de passe sont fournis
    	if(isset($nomPlante) && isset($nomCulture)){
		//test si le nom d'utilisateur est vide et si le mot passe est plus long que 6 caractères
		if($nomPlante=="" && $nomCulture == ""){
			$reply['error'] = 1;
			$reply['error_message'] = "Pas de pseudo/action";
		}
		else{
			$user = Outils_bd::plant_plant($nomCulture, $nomPlante);
			$reply["success"] = 1;
		}
    }
    else{
		$reply["error"] = 1;
		$reply["error_message"] = "Une erreur";
    }    
	echo json_encode($reply);    
?>
